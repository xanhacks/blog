---
title: "About"
date: 2020-06-06T14:26:43+02:00
draft: false
---

Hello, I'm **xanhacks**, an IT student passionate about infosec ! :fr:

I hope you will learn many interesting things on my blog/website. :smile:

{{< image src="../gif/save_the_world.webp" alt="GIF Saving the world !" position="center" style="border-radius: 2px; margin-top: 20px;" >}}

Go [back]({{< baseurl >}}) !
