---
title: "Best Firefox Add-ons"
date: 2020-07-02T10:31:58+02:00
draft: false
toc: true
images:
tags:
  - firefox
  - addons
  - privacy
  - opensource
---

## Getting started

There's the list of my favorites add-ons for Firefox split in 4 categories.

- **Utility :** Save time and upgrade your productivity.

- **Privacy :** Improve your privacy.

- **Web Tools :** Tools to helps you in web development / web security.

- **My add-ons :** Add-ons that I have developed.

## Utility

**Buster: Captcha Solver for Humans**

Buster is an open source add-on that solve Google captcha for you.

- https://addons.mozilla.org/en-GB/firefox/addon/buster-captcha-solver

- https://github.com/dessant/buster

{{< image src="../../posts-img/firefox-addons/buster.png" alt="Buster" position="center" style="border-radius: 2px;" >}}

**Copy PlainText**

Copy PlainText allows you to copy text without any formatting.

- https://addons.mozilla.org/en-GB/firefox/addon/copy-plaintext/

{{< image src="../../posts-img/firefox-addons/copyplaintext.png" alt="Copy PlainText" position="center" style="border-radius: 2px;" >}}

**Dark Reader**

One of my favorite add-on of this list.

Dark reader enable dark mode for every website you visit. You can enable/disable this add-on at any moment, or add a website to a whitelist. Open source.

- https://addons.mozilla.org/en-GB/firefox/addon/darkreader/

- https://github.com/darkreader/darkreader

{{< image src="../../posts-img/firefox-addons/darkreader.png" alt="Dark Reader" position="center" style="border-radius: 2px;" >}}

**Enhancer for Youtube**

I don't use Youtube anymore but this add-on is still awesome.

You can change the speed of the video to what you want, for example you can set the speed to 1.12x, not only 1.25x or 1.5x.

- https://addons.mozilla.org/en-GB/firefox/addon/enhancer-for-youtube/

{{< image src="../../posts-img/firefox-addons/youtubeenhancer.png" alt="Enhancer for Youtube" position="center" style="border-radius: 2px;" >}}

## Privacy

**AdBlocker Ultimate**

This add-on removes almost all the ads.

- https://addons.mozilla.org/en-GB/firefox/addon/adblocker-ultimate/

{{< image src="../../posts-img/firefox-addons/adblock.png" alt="Adblock" position="center" style="border-radius: 2px;" >}}

**Cookie AutoDelete**

Automatically delete your cookies. You can add different websites to a whitelist. Open source add-on.

- https://addons.mozilla.org/en-GB/firefox/addon/cookie-autodelete/

- https://github.com/Cookie-AutoDelete/Cookie-AutoDelete

{{< image src="../../posts-img/firefox-addons/cookieautodelete.png" alt="Cookie Autodelete" position="center" style="border-radius: 2px;" >}}

**Decentraleyes**

Decentraleyes intercepts the traffic, finds supported resources locally, and injects them into the environment. Open source add-on.

- https://addons.mozilla.org/en-GB/firefox/addon/decentraleyes/

- https://git.synz.io/Synzvato/decentraleyes

{{< image src="../../posts-img/firefox-addons/decentraleyes.png" alt="Decentraleyes" position="center" style="border-radius: 2px;" >}}

**DuckDuckGo Privacy Essentials**

This add-on helps you to fight against trackers and gives a privacy rank to all the websites you visit. Open source as well.

- https://addons.mozilla.org/en-GB/firefox/addon/duckduckgo-for-firefox/

- https://github.com/duckduckgo/duckduckgo-privacy-extension

{{< image src="../../posts-img/firefox-addons/duckduckgo.png" alt="DuckDuckGo" position="center" style="border-radius: 2px;" >}}

**HTTPS Everywhere**

This add-on helps you to improve your security by using automatically HTTPS. Open source as well.

- https://addons.mozilla.org/en-GB/firefox/addon/https-everywhere/

- https://github.com/EFForg/https-everywhere

{{< image src="../../posts-img/firefox-addons/httpseverywhere.png" alt="HTTPS Everywhere" position="center" style="border-radius: 2px;" >}}

**Invidition**

This add-on transform Youtube's and Twitter's URLs into URLs of alternatives platforms like Invidious and Nitter. Personally, I use Invidious everyday instead of Youtube. Open source as well.

- https://addons.mozilla.org/en-GB/firefox/addon/invidition/

- https://gitlab.com/Booteille/invidition

{{< image src="../../posts-img/firefox-addons/invidition.png" alt="Invidition" position="center" style="border-radius: 2px;" >}}


**Privacy Badger**

Another awesome extension that block unwanted trackers. Open source as well.

- https://addons.mozilla.org/en-GB/firefox/addon/privacy-badger17/

- https://github.com/EFForg/privacybadger/

{{< image src="../../posts-img/firefox-addons/privacybadger.png" alt="Privacy Badger" position="center" style="border-radius: 2px;" >}}

## Web Tools

**Cookie Manager**

This add-on helps you to manage (edit / add / remove) our cookies.

- https://addons.mozilla.org/en-GB/firefox/addon/cookie-manager-popup/

{{< image src="../../posts-img/firefox-addons/cookiemanager.png" alt="Cookie Manager" position="center" style="border-radius: 2px;" >}}

**Country Flags & IP Whois**

This add-on displays the country flag of website's server location.

- https://addons.mozilla.org/en-GB/firefox/addon/country-flags-ip-whois/

- https://github.com/andy-portmen/country-flags

{{< image src="../../posts-img/firefox-addons/countryflag.png" alt="Country Flag" position="center" style="border-radius: 2px;" >}}

**FoxyProxy Standard**

Create and change easily between our proxies. This extension is open source.

- https://addons.mozilla.org/en-GB/firefox/addon/foxyproxy-standard/

- https://github.com/foxyproxy/firefox-extension

{{< image src="../../posts-img/firefox-addons/foxyproxy.png" alt="FoxyProxy" position="center" style="border-radius: 2px;" >}}

**User-Agent Switcher and Manager**

Switch easily between tons of User Agents. This extension is open source.

- https://addons.mozilla.org/en-GB/firefox/addon/user-agent-string-switcher/

- https://github.com/ray-lothian/UserAgent-Switcher/

{{< image src="../../posts-img/firefox-addons/useragentswitcher.png" alt="User Agent Switcher" position="center" style="border-radius: 2px;" >}}

**Wappalyzer**

Superb add-on that identify the web technologies (and versions) of the web page you're currently on. This add-on is open source.

- https://addons.mozilla.org/en-GB/firefox/addon/wappalyzer/

- https://github.com/aliasio/wappalyzer

{{< image src="../../posts-img/firefox-addons/wappalyzer.png" alt="Wappalyzer" position="center" style="border-radius: 2px;" >}}

## My add-ons

**ShowMyIP**

Show you current IP address. This add-on is open source.

- https://addons.mozilla.org/en-US/firefox/addon/showmyip/

- https://gitlab.com/xanhacks/showmyip

{{< image src="../../posts-img/firefox-addons/showmyip.png" alt="ShowMyIP" position="center" style="border-radius: 2px;" >}}

**youtube-nocookie**

Change youtube's URL into youtube-nocookie's URL. This addon is open source.

- https://addons.mozilla.org/en-US/firefox/addon/youtube-privacy/

- https://gitlab.com/xanhacks/youtube-nocookie

{{< image src="../../posts-img/firefox-addons/ytnocookie.png" alt="Youtube nocookie" position="center" style="border-radius: 2px;" >}}
