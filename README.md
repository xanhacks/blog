# xanhacks' blog

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

You can find an instance of my blog [here](https://xanhacks.gitlab.io/blog/).

## Installation


```bash
$ git clone --recurse-submodules https://gitlab.com/xanhacks/blog
```

## Run

[Hugo docs](https://gohugo.io/documentation/)

```bash
$ cd blog
$ hugo server
```

## Tech

| Name | Links |
| ------ | ------ |
| Hugo | [https://gohugo.io/](https://gohugo.io/) |
| Theme | [https://github.com/rhazdon/hugo-theme-hello-friend-ng](https://github.com/rhazdon/hugo-theme-hello-friend-ng) |

## License
[MIT](https://choosealicense.com/licenses/mit/)
